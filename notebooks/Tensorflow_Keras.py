# Databricks notebook source
# MAGIC %md 
# MAGIC # Tensorflow Keras in Databricks
# MAGIC 
# MAGIC This demo will uses a small dataset to show how to use TensorFlow Keras, and MLflow to develop a deep learning model in Databricks. 
# MAGIC 
# MAGIC It includes the following steps:
# MAGIC - Load and preprocess data
# MAGIC - Create a neural network model with TensorFlow Keras and integrated with MLFlow
# MAGIC 
# MAGIC ### Setup
# MAGIC - Databricks Runtime for Machine Learning 7.0 or above.

# COMMAND ----------

import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
import mlflow
import mlflow.keras
import mlflow.tensorflow
import numpy as np
import pandas as pd

from sklearn.datasets import fetch_california_housing
from sklearn.model_selection import train_test_split

# COMMAND ----------

# MAGIC %md ## Load and preprocess data from Delta Table
# MAGIC This example uses the California Housing dataset originally from `scikit-learn`. 

# COMMAND ----------

# MAGIC %sql
# MAGIC DESCRIBE HISTORY delta.`/mnt/data1/tables/california_housing`

# COMMAND ----------

sdf = spark.read.format("delta").option("versionAsOf", 1).load("/mnt/data1/tables/california_housing")
X = np.array(sdf.select("HouseAge","AveRooms", "AveBedrms", "Population", "AveOccup", "Latitude", "Longitude", "MedHouseVal").collect())
y = np.array(sdf.select("MedHouseVal").collect())

# COMMAND ----------

# Split 80/20 train-test
X_train, X_test, y_train, y_test = train_test_split(X,
                                                    y,
                                                    test_size=0.2)

# COMMAND ----------

# MAGIC %md ### Scale features
# MAGIC Feature scaling is important when working with neural networks. This notebook uses the `scikit-learn` function `StandardScaler`.

# COMMAND ----------

from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# COMMAND ----------

# MAGIC %md ## Create model

# COMMAND ----------

# MAGIC %md ### Create the neural network

# COMMAND ----------

def create_model():
  model = Sequential()
  model.add(Dense(20, input_dim=8, activation="relu"))
  model.add(Dense(20, activation="relu"))
  model.add(Dense(1, activation="linear"))
  return model

# COMMAND ----------

# MAGIC %md ### Compile the model

# COMMAND ----------

model = create_model()

model.compile(loss="mse",
              optimizer="Adam",
              metrics=["mse"])

# COMMAND ----------

# MAGIC %md ### Create callbacks

# COMMAND ----------

from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping

# In the following lines, replace <username> with your username.
experiment_log_dir = "/dbfs/user/yang.wang@databricks.com/tb"
checkpoint_path = "/dbfs/user/yang.wang@databricks.com/keras_checkpoint_weights.ckpt"

model_checkpoint = ModelCheckpoint(filepath=checkpoint_path, verbose=1, save_best_only=True)
early_stopping = EarlyStopping(monitor="loss", mode="min", patience=3)


import matplotlib.pyplot as plt
mlflow.tensorflow.autolog()
 
with mlflow.start_run(experiment_id=3245658605405057) as run:
  
  history = model.fit(X_train, y_train, validation_split=.2, epochs=20, callbacks=[model_checkpoint, early_stopping])
  
  # Save the run information to register the model later
  kerasURI = run.info.artifact_uri
  
  # Evaluate model on test dataset and log result
  mlflow.log_param("eval_result", model.evaluate(X_test, y_test)[0])
  
  # Plot predicted vs known values for a quick visual check of the model and log the plot as an artifact
  keras_pred = model.predict(X_test)
  plt.plot(y_test, keras_pred, "o", markersize=2)
  plt.xlabel("observed value")
  plt.ylabel("predicted value")
  plt.savefig("kplot.png")
  mlflow.log_artifact("kplot.png") 

# COMMAND ----------

# MAGIC %md ## Register the Model

# COMMAND ----------

# TODO
# In Dev workspace, develop the training notebook, use the dev data for the development, once finish, push the notebook to Bitbucket
# In Prod workspace, checkout the notebook from Bitbucket(Repo) and train the model with Prod Data
# Make a new nobebook to gegister the model to Centralised Model Registry (CMR), put the artifact_uri into this notebook
  # Need kerasURI = run.info.artifact_uri
# Once the model is in CMR with none state, the DS can make a request to transit the model to staging state
# The DS manager can approve the transit, and it will trigger the mlflow webhook, which run a CI/CD pipeline in Jenkins
# The pipeline will deploy the model to Test Workspace, use the notebook to fetch the model from CMR, and then test the model, if the model pass all the test, make a request to transit the model from staging  to production. The Test Workspace is using the service account. 
  # Also the CI/CD pipeline will also deploy the model to Test AML and test the Restful endpoint
# The DS manager can approve the transit again from staging to production, and it will trigger the mlflow webhook to deploy the model to Prod Workspace and Prod AML

# Check webhook recording again
  # Webhook should be create on the CMR
  # Registry webhooks only support HTTPS URLs, need to make jenkins to use https

# COMMAND ----------

import time
from mlflow.tracking.client import MlflowClient
client = MlflowClient()

model_name = "cal_housing_keras"
model_uri = kerasURI+"/model"
new_model_version = mlflow.register_model(model_uri, model_name)

# Registering the model takes a few seconds, so add a delay before continuing with the next cell
time.sleep(5)

# COMMAND ----------

model_version_infos = client.search_model_versions("name = '%s'" % model_name)
current_production_model_version = [model_version_info.version for model_version_info in model_version_infos if model_version_info.current_stage=='Production']
client.transition_model_version_stage(
  name=model_name,
  version=current_production_model_version[0],
  stage="Archived",
)

# COMMAND ----------

client.transition_model_version_stage(
  name=model_name,
  version=new_model_version.version,
  stage="Production",
)

# COMMAND ----------

# MAGIC %md ## Load the model for inference and make predictions

# COMMAND ----------

keras_model = mlflow.keras.load_model(f"models:/{model_name}/{new_model_version.version}")

keras_pred = keras_model.predict(X_test)
keras_pred

# COMMAND ----------

X_test[0]

# COMMAND ----------

keras_model.predict(np.array([X_test[0]]))

# COMMAND ----------

#Update
