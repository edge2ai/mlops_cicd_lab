import pyspark.sql.functions as F

def with_status(df):
    # Add status column
    return df.withColumn("status", F.lit("checked"))

